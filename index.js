function result() {
  var diemChuan = document.getElementById("txt-diem-chuan").value * 1;
  var diemThu1 = document.getElementById("txt-diem1").value * 1;
  var diemThu2 = document.getElementById("txt-diem2").value * 1;
  var diemThu3 = document.getElementById("txt-diem3").value * 1;
  var doiTuong = document.querySelector(
    'input[name="doi-tuong"]:checked'
  ).value;
  var khuVuc = document.querySelector('input[name="khu-vuc"]:checked').value;
  var diemTongket = 0;
  function tinhDiemCongKhuVuc(khuVuc) {
    if (khuVuc == "A") {
      return 2;
    }
    if (khuVuc == "B") {
      return 1;
    }
    if (khuVuc == "C") {
      return 0.5;
    }
  }
  function tinhDiemCongDoiTuong(doiTuong) {
    if (doiTuong == "A") {
      return 2.5;
    }
    if (doiTuong == "B") {
      return 1.5;
    }
    if (doiTuong == "C") {
      return 1;
    }
  }
  var diemCongKhuVuc = tinhDiemCongKhuVuc(khuVuc);
  var diemCongDoiTuong = tinhDiemCongDoiTuong(doiTuong);

  if (diemThu1 <= 0 || diemThu2 <= 0 || diemThu3 <= 0) {
    document.getElementById("result1").innerHTML = `Bạn không trúng tuyển`;
  } else {
    diemTongket =
      diemThu1 + diemThu2 + diemThu3 + diemCongDoiTuong + diemCongKhuVuc;

    // switch (doiTuong) {
    //   case "0":
    //     diemTongket = diemThu1 + diemThu2 + diemThu3;
    //     break;
    //   case "A":
    //     diemTongket = diemThu1 + diemThu2 + diemThu3 + 2.5;
    //     break;
    //   case "B":
    //     diemTongket = diemThu1 + diemThu2 + diemThu3 + 1.5;
    //     break;
    //   case "C":
    //     diemTongket = diemThu1 + diemThu2 + diemThu3 + 1;

    //     break;
    // }
    // switch (khuVuc) {
    //   case "0":
    //     diemTongket;
    //     break;
    //   case "A":
    //     diemTongket += 2;
    //     break;
    //   case "B":
    //     diemTongket += 1;
    //     break;
    //   case "C":
    //     diemTongket += 0.5;

    //     break;
    // }
    if (diemTongket >= diemChuan) {
      document.getElementById("result1").innerHTML = `Bạn đã trúng tuyển`;
    } else {
      document.getElementById("result1").innerHTML = `Bạn không trúng tuyển`;
    }
  }
}

// bai 2
function result2() {
  var soKw = document.getElementById("txt-kw").value * 1;
  var name = document.getElementById("txt-name").value;
  var tienDien = 0;
  function tinhTien50kwDau(soKw) {
    return 500 * soKw;
  }
  function tinhTien50kwKe(soKw) {
    return 50 * 500 + (soKw - 50) * 650;
  }
  function tinhTien100kwKe(soKw) {
    return 50 * 500 + 50 * 650 + (soKw - 100) * 850;
  }
  function tinhTien150kwKe(soKw) {
    return 50 * 500 + 50 * 650 + 100 * 850 + (soKw - 200) * 1100;
  }
  function tinhTienConLai(soKw) {
    return 50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (soKw - 350) * 1300;
  }

  if (soKw <= 50) {
    tienDien = tinhTien50kwDau(soKw);
  } else if (soKw <= 100) {
    tienDien = tinhTien50kwKe(soKw);
  } else if (soKw <= 200) {
    tienDien = tinhTien100kwKe(soKw);
  } else if (soKw <= 350) {
    tienDien = tinhTien150kwKe(soKw);
  } else {
    tienDien = tinhTienConLai(soKw);
  }

  document.getElementById(
    "result2"
  ).innerHTML = `Tiền điện ${name} cần trả là : ${tienDien}`;
}
